# QGIS Italia Risorse
Risorse da condividere mediante il plugin `Resource sharing`

procedimento per usare le risorse

1. Scaricare ed installare il plugin [Reosurce sharing](http://www.akbargumbira.com/qgis_resources_sharing/) in QGIS;
2. dopo l'installazione del plugin avviarlo e selezionare il tab `Settings` 
3. cliccare su `Add`, aggiungete in `URL` il link https://git.osgeo.org/gogs/qgisitalia/QGIS-Italia-Risorse.git, basta un copia incolla; in `Name` scrivete qualcosa che vi ricordi il repository - es: Risorse QGIS Italia -; infine `OK`; 
4. Ora nel tab `All` troverete tutte le risorse: `qgis_3-icon-ITA`,`microzonazione`,`carg`,`fdgc`,`sicod`,`n_arrows` 
5. Selezionate le risorse e cliccate su `install`;
6. dopo installazione e messaggio che è andato tutto bene, nella tab `installed` troverete la risorsa.
7. cliccando si `Open folder`, accederete alla cartella con tutte le risorse.

per maggiori dettagli: http://www.akbargumbira.com/qgis_resources_sharing/user/adding-repository.html

video: https://www.youtube.com/watch?time_continue=38&v=77fglr3C2TU


## Come usare le risorse

le risorse sono delle collezione di immagini SVG quindi le ritroverete in QGIS pronta per essere utilizzata.


-----

alcuni esempi:

![collezione](/collections/qgis_3-icon-ITA/preview/serie_icon_Q3_ITA.png)

----

![EVOLUZIONE](collections/qgis_3-icon-ITA/preview/evolution%20qgis_ita_storia.png)

